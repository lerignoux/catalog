**Important:**
*for the complete changelog, please refer to the website*




## [adminer-5.0.10](https://github.com/truecharts/charts/compare/adminer-5.0.9...adminer-5.0.10) (2023-09-07)

### Chore

- update container image tccr.io/truecharts/adminer to latest ([#12345](https://github.com/truecharts/charts/issues/12345))
  
  


## [adminer-5.0.9](https://github.com/truecharts/charts/compare/adminer-5.0.8...adminer-5.0.9) (2023-09-07)

### Chore

- update container image tccr.io/truecharts/adminer to latest ([#12337](https://github.com/truecharts/charts/issues/12337))
  
  


## [adminer-5.0.8](https://github.com/truecharts/charts/compare/adminer-5.0.7...adminer-5.0.8) (2023-09-07)

### Chore

- update container image tccr.io/truecharts/adminer to latest ([#12330](https://github.com/truecharts/charts/issues/12330))
  
  


## [adminer-5.0.7](https://github.com/truecharts/charts/compare/adminer-5.0.6...adminer-5.0.7) (2023-09-07)

### Chore

- update container image tccr.io/truecharts/adminer to latest ([#12324](https://github.com/truecharts/charts/issues/12324))
  
  


## [adminer-5.0.6](https://github.com/truecharts/charts/compare/adminer-5.0.5...adminer-5.0.6) (2023-08-16)

### Chore

- update container image tccr.io/truecharts/adminer to latest ([#11605](https://github.com/truecharts/charts/issues/11605))
  
  


## [adminer-5.0.5](https://github.com/truecharts/charts/compare/adminer-5.0.4...adminer-5.0.5) (2023-08-16)

### Chore

- update container image tccr.io/truecharts/adminer to latest ([#11597](https://github.com/truecharts/charts/issues/11597))
  
  


## [adminer-5.0.4](https://github.com/truecharts/charts/compare/adminer-5.0.3...adminer-5.0.4) (2023-08-16)

### Chore

- update container image tccr.io/truecharts/adminer to latest ([#11595](https://github.com/truecharts/charts/issues/11595))
  
  


## [adminer-5.0.3](https://github.com/truecharts/charts/compare/adminer-5.0.2...adminer-5.0.3) (2023-08-16)

### Chore

- update container image tccr.io/truecharts/adminer to latest ([#11589](https://github.com/truecharts/charts/issues/11589))
  
  


## [adminer-5.0.2](https://github.com/truecharts/charts/compare/adminer-5.0.1...adminer-5.0.2) (2023-08-16)

### Chore

- update container image tccr.io/truecharts/adminer to latest ([#11585](https://github.com/truecharts/charts/issues/11585))
  
  


## [adminer-5.0.1](https://github.com/truecharts/charts/compare/adminer-5.0.0...adminer-5.0.1) (2023-08-16)

### Chore

- update container image tccr.io/truecharts/adminer to latest ([#11582](https://github.com/truecharts/charts/issues/11582))
  
  



## [adminer-5.0.0](https://github.com/truecharts/charts/compare/adminer-4.0.40...adminer-5.0.0) (2023-07-31)

