

## [adminer-5.0.10](https://github.com/truecharts/charts/compare/adminer-5.0.9...adminer-5.0.10) (2023-09-07)

### Chore

- update container image tccr.io/truecharts/adminer to latest ([#12345](https://github.com/truecharts/charts/issues/12345))
  
  