

## [projectsend-8.0.6](https://github.com/truecharts/charts/compare/projectsend-8.0.5...projectsend-8.0.6) (2023-09-11)

### Chore

- update container image tccr.io/truecharts/projectsend to vlatest ([#12498](https://github.com/truecharts/charts/issues/12498))
  
  