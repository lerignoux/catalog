

## [ghostfolio-0.0.13](https://github.com/truecharts/charts/compare/ghostfolio-0.0.12...ghostfolio-0.0.13) (2023-09-17)

### Chore

- update container image tccr.io/truecharts/ghostfolio to v2.2.0 ([#12694](https://github.com/truecharts/charts/issues/12694))
  
  