

## [invidious-6.0.4](https://github.com/truecharts/charts/compare/invidious-6.0.3...invidious-6.0.4) (2023-09-16)

### Chore

- update container image tccr.io/truecharts/invidious to latest ([#12649](https://github.com/truecharts/charts/issues/12649))
  
  