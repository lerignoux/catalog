

## [static-web-server-1.0.1](https://github.com/truecharts/charts/compare/static-web-server-1.0.0...static-web-server-1.0.1) (2023-09-03)

### Chore

- update container image tccr.io/truecharts/static-web-server to v2.21.1 ([#12179](https://github.com/truecharts/charts/issues/12179))
  
  