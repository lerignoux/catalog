**Important:**
*for the complete changelog, please refer to the website*




## [rdesktop-5.0.79](https://github.com/truecharts/charts/compare/rdesktop-5.0.78...rdesktop-5.0.79) (2023-09-15)

### Chore

- update rdesktop ([#12629](https://github.com/truecharts/charts/issues/12629))
  
  


## [rdesktop-5.0.78](https://github.com/truecharts/charts/compare/rdesktop-5.0.77...rdesktop-5.0.78) (2023-09-15)

### Chore

- update rdesktop ([#12626](https://github.com/truecharts/charts/issues/12626))
  
  


## [rdesktop-5.0.77](https://github.com/truecharts/charts/compare/rdesktop-5.0.76...rdesktop-5.0.77) (2023-09-11)

### Chore

- update rdesktop ([#12483](https://github.com/truecharts/charts/issues/12483))
  
  


## [rdesktop-5.0.76](https://github.com/truecharts/charts/compare/rdesktop-5.0.75...rdesktop-5.0.76) (2023-09-11)

### Chore

- update rdesktop ([#12482](https://github.com/truecharts/charts/issues/12482))
  
  


## [rdesktop-5.0.75](https://github.com/truecharts/charts/compare/rdesktop-5.0.74...rdesktop-5.0.75) (2023-09-11)

### Chore

- update container image tccr.io/truecharts/rdesktop to latest ([#12480](https://github.com/truecharts/charts/issues/12480))
  
  


## [rdesktop-5.0.74](https://github.com/truecharts/charts/compare/rdesktop-5.0.73...rdesktop-5.0.74) (2023-09-11)

### Chore

- update rdesktop ([#12479](https://github.com/truecharts/charts/issues/12479))
  
  


## [rdesktop-5.0.73](https://github.com/truecharts/charts/compare/rdesktop-5.0.72...rdesktop-5.0.73) (2023-09-11)

### Chore

- update rdesktop ([#12477](https://github.com/truecharts/charts/issues/12477))
  
  


## [rdesktop-5.0.72](https://github.com/truecharts/charts/compare/rdesktop-5.0.71...rdesktop-5.0.72) (2023-09-08)

### Chore

- update rdesktop ([#12399](https://github.com/truecharts/charts/issues/12399))
  
  


## [rdesktop-5.0.71](https://github.com/truecharts/charts/compare/rdesktop-5.0.70...rdesktop-5.0.71) (2023-09-08)

### Chore

- update container image tccr.io/truecharts/rdesktop-arch-kde to latest ([#12394](https://github.com/truecharts/charts/issues/12394))
  
  


## [rdesktop-5.0.70](https://github.com/truecharts/charts/compare/rdesktop-5.0.69...rdesktop-5.0.70) (2023-09-08)

### Chore

- update rdesktop ([#12393](https://github.com/truecharts/charts/issues/12393))
  
  


## [rdesktop-5.0.69](https://github.com/truecharts/charts/compare/rdesktop-5.0.68...rdesktop-5.0.69) (2023-09-04)

### Chore
