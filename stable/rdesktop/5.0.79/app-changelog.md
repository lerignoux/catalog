

## [rdesktop-5.0.79](https://github.com/truecharts/charts/compare/rdesktop-5.0.78...rdesktop-5.0.79) (2023-09-15)

### Chore

- update rdesktop ([#12629](https://github.com/truecharts/charts/issues/12629))
  
  