

## [libremdb-3.0.89](https://github.com/truecharts/charts/compare/libremdb-3.0.88...libremdb-3.0.89) (2023-09-17)

### Chore

- update container image tccr.io/truecharts/libremdb to latest ([#12703](https://github.com/truecharts/charts/issues/12703))
  
  