

## [jupyter-5.0.24](https://github.com/truecharts/charts/compare/jupyter-5.0.23...jupyter-5.0.24) (2023-09-17)

### Chore

- update jupyter ([#12686](https://github.com/truecharts/charts/issues/12686))
  
  