

## [homepage-4.0.5](https://github.com/truecharts/charts/compare/homepage-4.0.4...homepage-4.0.5) (2023-09-16)

### Chore

- update container image tccr.io/truecharts/homepage to v0.6.33 ([#12677](https://github.com/truecharts/charts/issues/12677))
  
  