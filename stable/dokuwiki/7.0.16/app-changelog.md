

## [dokuwiki-7.0.16](https://github.com/truecharts/charts/compare/dokuwiki-7.0.15...dokuwiki-7.0.16) (2023-09-15)

### Chore

- update container image tccr.io/truecharts/dokuwiki to v20230404.1.0 ([#12628](https://github.com/truecharts/charts/issues/12628))
  
  