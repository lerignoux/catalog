

## [webtop-6.0.93](https://github.com/truecharts/charts/compare/webtop-6.0.92...webtop-6.0.93) (2023-09-15)

### Chore

- update container image tccr.io/truecharts/webtop-arch-xfce to latest ([#12633](https://github.com/truecharts/charts/issues/12633))
  
  