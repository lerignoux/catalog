**Important:**
*for the complete changelog, please refer to the website*




## [webtop-6.0.93](https://github.com/truecharts/charts/compare/webtop-6.0.92...webtop-6.0.93) (2023-09-15)

### Chore

- update container image tccr.io/truecharts/webtop-arch-xfce to latest ([#12633](https://github.com/truecharts/charts/issues/12633))
  
  


## [webtop-6.0.92](https://github.com/truecharts/charts/compare/webtop-6.0.91...webtop-6.0.92) (2023-09-15)

### Chore

- update webtop ([#12630](https://github.com/truecharts/charts/issues/12630))
  
  


## [webtop-6.0.91](https://github.com/truecharts/charts/compare/webtop-6.0.90...webtop-6.0.91) (2023-09-12)

### Chore

- update webtop ([#12517](https://github.com/truecharts/charts/issues/12517))
  
  


## [webtop-6.0.90](https://github.com/truecharts/charts/compare/webtop-6.0.89...webtop-6.0.90) (2023-09-12)

### Chore

- update webtop ([#12515](https://github.com/truecharts/charts/issues/12515))
  
  


## [webtop-6.0.89](https://github.com/truecharts/charts/compare/webtop-6.0.88...webtop-6.0.89) (2023-09-12)

### Chore

- update webtop ([#12512](https://github.com/truecharts/charts/issues/12512))
  
  


## [webtop-6.0.88](https://github.com/truecharts/charts/compare/webtop-6.0.87...webtop-6.0.88) (2023-09-12)

### Chore

- update container image tccr.io/truecharts/webtop-alpine-i3 to latest ([#12509](https://github.com/truecharts/charts/issues/12509))
  
  


## [webtop-6.0.87](https://github.com/truecharts/charts/compare/webtop-6.0.86...webtop-6.0.87) (2023-09-09)

### Chore

- update container image tccr.io/truecharts/webtop-arch-kde to latest ([#12407](https://github.com/truecharts/charts/issues/12407))
  
  


## [webtop-6.0.86](https://github.com/truecharts/charts/compare/webtop-6.0.85...webtop-6.0.86) (2023-09-08)

### Chore

- update webtop ([#12395](https://github.com/truecharts/charts/issues/12395))
  
  


## [webtop-6.0.85](https://github.com/truecharts/charts/compare/webtop-6.0.84...webtop-6.0.85) (2023-09-07)

### Chore

- update container image tccr.io/truecharts/webtop-arch-kde to latest ([#12363](https://github.com/truecharts/charts/issues/12363))
  
  


## [webtop-6.0.84](https://github.com/truecharts/charts/compare/webtop-6.0.83...webtop-6.0.84) (2023-09-06)

### Chore

- update container image tccr.io/truecharts/webtop-alpine-icewm to latest ([#12283](https://github.com/truecharts/charts/issues/12283))
  
  


## [webtop-6.0.83](https://github.com/truecharts/charts/compare/webtop-6.0.82...webtop-6.0.83) (2023-09-05)

### Chore
