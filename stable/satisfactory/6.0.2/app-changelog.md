

## [satisfactory-6.0.2](https://github.com/truecharts/charts/compare/satisfactory-6.0.1...satisfactory-6.0.2) (2023-09-17)

### Chore

- update container image tccr.io/truecharts/satisfactory to v1.5.3 ([#12704](https://github.com/truecharts/charts/issues/12704))
  
  