

## [boinc-6.0.7](https://github.com/truecharts/charts/compare/boinc-6.0.6...boinc-6.0.7) (2023-09-12)

### Chore

- update container image tccr.io/truecharts/boinc to latest ([#12525](https://github.com/truecharts/charts/issues/12525))
  
  