**Important:**
*for the complete changelog, please refer to the website*




## [searxng-4.0.27](https://github.com/truecharts/charts/compare/searxng-4.0.26...searxng-4.0.27) (2023-09-17)

### Chore

- update container image tccr.io/truecharts/searxng to latest ([#12701](https://github.com/truecharts/charts/issues/12701))
  
  


## [searxng-4.0.26](https://github.com/truecharts/charts/compare/searxng-4.0.25...searxng-4.0.26) (2023-09-16)

### Chore

- update container image tccr.io/truecharts/searxng to latest ([#12668](https://github.com/truecharts/charts/issues/12668))
  
  


## [searxng-4.0.25](https://github.com/truecharts/charts/compare/searxng-4.0.24...searxng-4.0.25) (2023-09-16)

### Chore

- update container image tccr.io/truecharts/searxng to latest ([#12654](https://github.com/truecharts/charts/issues/12654))
  
  


## [searxng-4.0.24](https://github.com/truecharts/charts/compare/searxng-4.0.23...searxng-4.0.24) (2023-09-16)

### Chore

- update container image tccr.io/truecharts/searxng to latest ([#12650](https://github.com/truecharts/charts/issues/12650))
  
  


## [searxng-4.0.23](https://github.com/truecharts/charts/compare/searxng-4.0.22...searxng-4.0.23) (2023-09-15)

### Chore

- update container image tccr.io/truecharts/searxng to latest ([#12625](https://github.com/truecharts/charts/issues/12625))
  
  


## [searxng-4.0.22](https://github.com/truecharts/charts/compare/searxng-4.0.21...searxng-4.0.22) (2023-09-15)

### Chore

- update container image tccr.io/truecharts/searxng to latest ([#12618](https://github.com/truecharts/charts/issues/12618))
  
  


## [searxng-4.0.21](https://github.com/truecharts/charts/compare/searxng-4.0.20...searxng-4.0.21) (2023-09-15)

### Chore

- update container image tccr.io/truecharts/searxng to latest ([#12616](https://github.com/truecharts/charts/issues/12616))
  
  


## [searxng-4.0.20](https://github.com/truecharts/charts/compare/searxng-4.0.19...searxng-4.0.20) (2023-09-14)

### Chore

- update container image tccr.io/truecharts/searxng to latest ([#12602](https://github.com/truecharts/charts/issues/12602))
  
  


## [searxng-4.0.19](https://github.com/truecharts/charts/compare/searxng-4.0.18...searxng-4.0.19) (2023-09-13)

### Chore

- update container image tccr.io/truecharts/searxng to latest ([#12576](https://github.com/truecharts/charts/issues/12576))
  
  


## [searxng-4.0.18](https://github.com/truecharts/charts/compare/searxng-4.0.17...searxng-4.0.18) (2023-09-13)

### Chore

- update container image tccr.io/truecharts/searxng to latest ([#12561](https://github.com/truecharts/charts/issues/12561))
  
  


## [searxng-4.0.17](https://github.com/truecharts/charts/compare/searxng-4.0.16...searxng-4.0.17) (2023-09-13)

### Chore
