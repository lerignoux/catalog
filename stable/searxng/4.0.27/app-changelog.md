

## [searxng-4.0.27](https://github.com/truecharts/charts/compare/searxng-4.0.26...searxng-4.0.27) (2023-09-17)

### Chore

- update container image tccr.io/truecharts/searxng to latest ([#12701](https://github.com/truecharts/charts/issues/12701))
  
  