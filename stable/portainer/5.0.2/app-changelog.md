

## [portainer-5.0.2](https://github.com/truecharts/charts/compare/portainer-5.0.1...portainer-5.0.2) (2023-09-08)

### Chore

- update portainer to v2.19.0 (minor) ([#12372](https://github.com/truecharts/charts/issues/12372))
  
  