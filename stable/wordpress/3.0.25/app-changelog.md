

## [wordpress-3.0.25](https://github.com/truecharts/charts/compare/wordpress-3.0.24...wordpress-3.0.25) (2023-09-15)

### Chore

- update container image tccr.io/truecharts/wordpress to v6.3.1 ([#12620](https://github.com/truecharts/charts/issues/12620))
  
  