

## [audiobookshelf-6.0.3](https://github.com/truecharts/charts/compare/audiobookshelf-6.0.2...audiobookshelf-6.0.3) (2023-09-13)

### Chore

- update container image tccr.io/truecharts/audiobookshelf to v2.4.2 ([#12579](https://github.com/truecharts/charts/issues/12579))
  
  