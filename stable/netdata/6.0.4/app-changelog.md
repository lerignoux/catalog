

## [netdata-6.0.4](https://github.com/truecharts/charts/compare/netdata-6.0.3...netdata-6.0.4) (2023-09-12)

### Chore

- update container image tccr.io/truecharts/netdata to v1.42.3 ([#12510](https://github.com/truecharts/charts/issues/12510))
  
  