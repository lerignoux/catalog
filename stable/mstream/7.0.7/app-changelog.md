

## [mstream-7.0.7](https://github.com/truecharts/charts/compare/mstream-7.0.6...mstream-7.0.7) (2023-09-13)

### Chore

- update container image tccr.io/truecharts/mstream to v5.11.4 ([#12548](https://github.com/truecharts/charts/issues/12548))
  
  