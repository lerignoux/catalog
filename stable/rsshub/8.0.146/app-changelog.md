

## [rsshub-8.0.146](https://github.com/truecharts/charts/compare/rsshub-8.0.145...rsshub-8.0.146) (2023-09-17)

### Chore

- update container image tccr.io/truecharts/rsshub to latest ([#12706](https://github.com/truecharts/charts/issues/12706))
  
  