

## [metabase-0.0.3](https://github.com/truecharts/charts/compare/metabase-0.0.2...metabase-0.0.3) (2023-09-08)

### Chore

- update container image tccr.io/truecharts/metabase to v0.47.1 ([#12388](https://github.com/truecharts/charts/issues/12388))
  
  