

## [grafana-9.0.15](https://github.com/truecharts/charts/compare/grafana-9.0.14...grafana-9.0.15) (2023-09-16)

### Chore

- update container image tccr.io/truecharts/grafana to v10.1.1 ([#12643](https://github.com/truecharts/charts/issues/12643))
  
  