

## [kube-state-metrics-3.0.16](https://github.com/truecharts/charts/compare/kube-state-metrics-3.0.15...kube-state-metrics-3.0.16) (2023-09-15)

### Chore

- update container image tccr.io/truecharts/kube-state-metrics to v2.10.0 ([#12638](https://github.com/truecharts/charts/issues/12638))
  
  