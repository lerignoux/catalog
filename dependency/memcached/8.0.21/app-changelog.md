

## [memcached-8.0.21](https://github.com/truecharts/charts/compare/memcached-8.0.20...memcached-8.0.21) (2023-09-17)

### Chore

- update container image tccr.io/truecharts/memcached to v1.6.21 ([#12693](https://github.com/truecharts/charts/issues/12693))
  
  