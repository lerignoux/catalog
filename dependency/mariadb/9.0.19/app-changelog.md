

## [mariadb-9.0.19](https://github.com/truecharts/charts/compare/mariadb-9.0.18...mariadb-9.0.19) (2023-09-16)

### Chore

- update container image tccr.io/truecharts/mariadb to v11.0.3 ([#12651](https://github.com/truecharts/charts/issues/12651))
  
  