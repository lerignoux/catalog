

## [mongodb-8.0.15](https://github.com/truecharts/charts/compare/mongodb-8.0.14...mongodb-8.0.15) (2023-09-17)

### Chore

- update container image tccr.io/truecharts/mongodb to v6.0.10 ([#12685](https://github.com/truecharts/charts/issues/12685))
  
  