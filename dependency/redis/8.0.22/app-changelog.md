

## [redis-8.0.22](https://github.com/truecharts/charts/compare/redis-8.0.21...redis-8.0.22) (2023-09-16)

### Chore

- update container image tccr.io/truecharts/redis to v7.2.1 ([#12641](https://github.com/truecharts/charts/issues/12641))
  
  